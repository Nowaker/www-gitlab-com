---
layout: markdown_page
title: "GitLab Direction"
---

This page describes the direction and roadmap for GitLab.
It is organized from the short to the long term.

## Your contributions

GitLab's direction is determined by the code that is sent by our [contributors](http://contributors.gitlab.com/).
We continually merge code to be released in the next version.
Contributing is the best way to get a feature you want included.
On [our issue tracker for CE](https://gitlab.com/gitlab-org/gitlab-ce/issues)
and [EE](https://gitlab.com/gitlab-org/gitlab-ee/issues),
many requests are made for features and changes to GitLab.
The ones with the
[status accepting merge requests](https://gitlab.com/gitlab-org/gitlab-ce/issues?milestone_id=&scope=all&sort=created_desc&state=opened&utf8=%E2%9C%93&assignee_id=&author_id=&milestone_title=&label_name=Accepting+Merge+Requests&weight=)
are pre-approved.
Of course before any code is merged it still has to meet the
[contribution acceptance criteria](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#contribution-acceptance-criteria).

## What our customers want

At GitLab the company we try to make what our users and we need (many of us are or used to be developers).
If a customer requests a feature, it carries extra weight.
Due to our short release cycle we can ship simple feature requests (for example an API extension) within one to two months.

## Previous releases

On our [release list page](https://about.gitlab.com/release-list/) you can find an overview of the most important features of recent releases and a links to the release blog posts.

## Next releases (automatically generated)

GitLab releases a new version every single month on the 22nd.
Note that we often move things around, do things that are not listed and don't do things that are listed.
This page is always in draft, some of the things on it might not ever be in GitLab.
New Products are indicated with 'New Product' in the issue title, this is our best estimate of what will be new products, it is not definitive.
Also the list below not include any contributions from outside GitLab the company.
The bullets list the tentpole features; the most important features of upcoming releases.
This is not an authoritative list of upcoming releases - it just reflects current [milestones](https://gitlab.com/groups/gitlab-org/milestones).
The CE and EE to the right of the version number link to all planned issues for that version.

The milestone, as seen below, is the intended date that the listed features get shipped.

<%= direction %>

## Wishlist <a name="wishlist"></a>

Below are features we'd really like to see in GitLab.
This list is not prioritized. We invite everyone to join the discussion by clicking the wishlist item that is of interest to you.
Feel free to comment, vote up or down any issue or just follow the conversation.  For GitLab sales, please add a link to the account in Salesforce.com that has expressed interest in a wishlist feature.
We very much welcome contributions that implement any of these things.

### Major Wins

<%= wishlist["major wins"] %>

### Usability

<%= wishlist["usability"] %>

### Code Review

<%= wishlist["code review"] %>

### Issue tracker

<%= wishlist["issues"] %>

### Version Control for Everything

<%= wishlist["vcs for everything"] %>

### Performance

<%= wishlist["Performance"] %>

### CI

<%= wishlist["CI"] %>

### Pages

<%= wishlist["pages"] %>

### Container Registry

<%= wishlist["container registry"] %>

### Moonshots

- [.git namespace with onename](https://gitlab.com/gitlab-org/gitlab-ce/issues/4232)
- [Commit bots for code cleanup](https://gitlab.com/gitlab-org/gitlab-ce/issues/22393)
<%= wishlist["moonshots"] %>

### New Products

<%= wishlist["ee product"] %>

### Meta issues

We use meta issues to collect similar feature proposals, so that we can
prioritize and review easily.

- [Code review](https://gitlab.com/gitlab-org/gitlab-ce/issues/19049)
- [Issue Board](https://gitlab.com/gitlab-org/gitlab-ce/issues/21365)
- [User management](https://gitlab.com/gitlab-org/gitlab-ce/issues/19860)
- [Moderation tools](https://gitlab.com/gitlab-org/gitlab-ce/issues/19313)
- [Wiki improvements](https://gitlab.com/gitlab-org/gitlab-ce/issues/19276)
- [Features for open source projects](https://gitlab.com/gitlab-org/gitlab-ce/issues/8938)

## Scope <a name="scope"></a>

[Our vision](#vision) is the need for an integrated set of tools for the software development lifecycle based on convention over configuration.
To achieve this we plan to ship the following stack of tools in our Omnibus package:

1. **Idea** (Chat) => [Mattermost](http://www.mattermost.org/) ships with GitLab
1. **Issue** (Tracker) => GitLab Issues ships with GitLab
1. **Plan** (Board) => GitLab Issue Board
1. **Code** (IDE) => [Koding integrates with GitLab](https://about.gitlab.com/2016/07/26/koding-and-gitlab-integrated/)
1. **Commit** (Repo) => GitLab Repositories ships with GitLab
1. **Test** (CI) => [GitLab Continuous Integration](https://about.gitlab.com/gitlab-ci/), and [Container Registry](https://about.gitlab.com/2016/05/23/gitlab-container-registry/) ship with GitLab. We would love to add support for [CodeClimate](https://gitlab.com/gitlab-org/gitlab-ce/issues/4044) and have an extended [vision for CI and CD](https://about.gitlab.com/direction/cicd/).
1. **Review** (MR) => GitLab Merge Requests ships with GitLab
1. **Staging** (CD) => [GitLab Deploy](https://docs.gitlab.com/ce/ci/environments.html) already ship with GitLab and we plan to ship with [Review Apps](https://gitlab.com/gitlab-org/gitlab-ce/issues/20255)
1. **Production** (Chatops) => We plan to ship with a [Slack bot](https://gitlab.com/gitlab-org/gitlab-ce/issues/22182) and [Cog integration](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/1412)
1. **Feedback** (Cycle Analytics) => We plan to ship with [Cycle Analytics](https://gitlab.com/gitlab-org/gitlab-ee/issues/847) and [Prometheus](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/1481)

### Outside our scope

1. **PaaS** although we do want to use [GitLab Deploy](https://gitlab.com/gitlab-org/gitlab-ce/issues/3286) to deploy to CloudFoundry, OpenStack, OpenShift, Kubernetes, Mesos DCOS, Docker Swarm, Atlas/Terraform, [Nomad](https://nomadproject.io/), [Deis](http://deis.io/), [Convox](http://www.convox.com/), [Flynn](https://flynn.io/), [Tutum](https://www.tutum.co/), [GiantSwarm](https://giantswarm.io/), [Rancher](https://github.com/rancher/rancher/blob/master/README.md)
1. **Configuration management** although we do want to upload cookbooks, manifests, playbooks, and modules for respectively Chef, Puppet, Ansible, and Salt.
1. **Distributed configuration stores** Consul, Etcd, Zookeeper, Vault
1. **Container configuration agents** [ContainerPilot](http://container-solutions.com/containerpilot-on-mantl/), [Orchestrator-agent](https://www.percona.com/blog/2016/04/13/orchestrator-agent-how-to-recover-a-mysql-database/)
1. **Log monitoring** ELK stack, Graylog, Splunk
1. **Infrastructure monitoring** CheckMK, Nagios, Sensu
1. **Error monitoring** Sentry, Airbrake, Bugsnag
1. **Network** Openflow, VMware NSX, Cisco ACI
1. **Incident notification** Pagerduty, Pingdom
1. **Network security** Nmap, rkhunter, Metasploit, Snort, OpenVAS, OSSEC

## Vision <a name="vision"></a>

From development teams to marketing organizations, everyone needs to collaborate on
digital content. Content should be open to suggestions by a wide number of
potential contributors. Open contribution can be achieved by using a mergeable
file format and distributed version control. The vision of GitLab is to **allow
everyone to collaborate on all digital content** so people can cooperate
effectively and achieve better results, faster.

Ideas flow though many stages before they are realized. An idea originates in a chat discussion, an issue is created, it is planned in a sprint, coded in an IDE, committed to
version control, tested by CI, code reviewed, deployed, checked and documented. Stitching all these stages of the software developement lifecycle together can be done in many different ways. You can have a marketplace of
proprietary apps from different suppliers or use a suite of products developed
in isolation. We believe that an **integrated set of tools for the software development lifecycle based on convention over configuration** offers a superior user experience. The
advantage can be quoted from the [Wikipedia page for convention over
configuration](https://en.wikipedia.org/wiki/Convention_over_configuration):
"decrease the number of decisions that developers need to make, gaining
simplicity, and not necessarily losing flexibility". In GitLab you only have to
specify unconventional aspects of your workflow.
The happy path is **frictionless from idea to production**.

We admire other convention over configuration tools like [Ruby on
Rails](http://rubyonrails.org/) (that doctrine of which perfectly describe the [value of integrated systems](http://rubyonrails.org/doctrine#integrated-systems)), [Ember](http://emberjs.com/), and
[Heroku](https://www.heroku.com/), and strive to offer the same advantages for a
continuous delivery of software.

We prefer to offer an integrated set of tools instead of a network of services or offering plugins for the following reasons:

1. We think an integrated set of tools provides a better user experience that a modular approach, as detailed by [this article from Stratechery](https://stratechery.com/2013/clayton-christensen-got-wrong/).
1. The open source nature of GitLab ensures that that we can combine great open source products.
1. Everyone can contribute to create a feature set that is [more complete than other tools](https://about.gitlab.com/comparison/). We'll focus on making all the parts work well together to create a better user experience.
1. Because GitLab is open source the enhancements can become [part of
the codebase instead](http://doc.gitlab.com/ce/project_services/project_services.html) of being external. This ensures the automated tests for all
functionality are continually run, ensuring that additions keep working work. This is contrast to externally maintained plugins that might not be updated.
1. Having the enhancements as part of the codebase also
ensures GitLab can continue to evolve with its additions instead of being bound
to an API that is hard to change and that resists refactoring. Refactoring is essential to maintaining a codebase that is easy to contribute to.
1. Many people use GitLab on-premises, for such situations it is much easier to install one tool than installing and integrating many tools.
1. GitLab is used by many large organizations with complex purchasing processes, having to buy only one subscription simplifies their purchasing.