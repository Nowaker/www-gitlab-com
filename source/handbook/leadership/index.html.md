---
layout: markdown_page
title: Leadership
---

## Guidelines

- As a leader, team members will follow your behavior, always do the right thing.
- Behavior should be consistent inside and outside the company, don't fake it outside, just do the right thing inside the company as well.
- In tough times people will put it in their best efforts when they do it for each other.
- When times are great be a voice of moderation, when times are bad be a voice of hope.
- A span of control should be around 7, from 4 to 10. Lower means too many generals and too few soldiers. Higher means you don't have time for 1:1's anymore.
- We work async, lead by example and make sure people understand that things need to be written down in issues as they happen.
- Start meetings on time, be on time yourself, don't ask if everyone is there, and don't punish people that have shown up on time by waiting for anyone.
- We have a simple hierarchy, everyone has one boss that is experienced in their subject matter. Matrix organizations are too hard.
- We don't have project managers. The individual contributors need to manage themselves.
- We are not a democratic or consensus driven company. People are encouraged to give their comments and opinions. But in the end one person decides the matter after have listened to all the feedback.
- It is encouraged to disagree and have a constructive confrontation, but it is "disagree & commit", when a decision is taken you make it happen.
- We give feedback, lots of it, don't hold back on suggestions to improve.
- If you praise someone try to do it in front of an audience, if you give suggestions to improve do it 1 on 1.
- If you meet external people always ask what they think we should improve.
- As soon as you know you'll have to let someone go, do it immediately. The team member is entitled to knowing where they stand. Delaying it for days or weeks causes problems with confidentiality (find out that they will be let go), causation (attributing it to another reason), and escalation (the working relation is probably going downhill).
- When someone says they are considering to quit drop everything and listen to them by asking questions to find out what their concerns are. If you delay the person will not feel valued and the decision will be irreversible.
- People should not be raised or given a title because they ask for it or threaten to quit. We should proactively raise and promote people without people asking. If you do it when people ask you are disadvantaging people that don't ask and you'll end up with many more people asking.
- Don't refer to team members [as family](https://hbr.org/2014/06/your-company-is-not-a-family). It is great that our team feels like a close-knit group and we should encourage that. But families don't have an an [offboarding process](https://about.gitlab.com/handbook/offboarding/).
- We avoid meetings because those aren't supporting the asynchronous work flow, are hard to conduct due to timezone differences and are limited only to those attending them, making it harder to share.
- Praise and credit the work of your reports to the rest of the company, never present it as your own. This and many other great lessons in [an ask metafilter thread worth reading](http://ask.metafilter.com/300002/My-best-manager-did-this).
- Try to be aware of your [cognitive biases](https://betterhumans.coach.me/cognitive-bias-cheat-sheet-55a472476b18).


## Articles

- [eShares Manager’s FAQ](https://readthink.com/a-managers-faq-35858a229f84)
- [eShares How to hire](https://blog.esharesinc.com/how-to-hire-34f4ded5f176)
- [How Facebook Tries to Prevent Office Politics](https://hbr.org/2016/06/how-facebook-tries-to-prevent-office-politics)
- [The Management Myth](http://www.theatlantic.com/magazine/archive/2006/06/the-management-myth/304883/)
- [Later Stage Advice for Startups](http://themacro.com/articles/2016/07/later-stage-advice-for-startups/)
- [Mental Models I Find Repeatedly Useful](https://medium.com/@yegg/mental-models-i-find-repeatedly-useful-936f1cc405d)
- [This Is The Most Difficult Skill For CEOs To Learn](http://www.businessinsider.com/whats-the-most-difficult-ceo-skill-managing-your-own-psychology-2011-4)

## Books

- High output management - Andrew Grove ([top 10](https://getlighthouse.com/blog/andy-grove-quotes-leadership-high-output-management/))
- The Hard thing about hard things - Ben Horowitz
- [The score takes care of itself - Bill Walsh](http://coachjacksonspages.com/The%20Score%20Takes%20Care.pdf)

## No matrix organization

We believe everyone deserves to report to exactly one person that knows and understands what you do day to day.
We don't want a matrix organizations where you work with a lead day to day but formally report to someone else.
The advantage of our structure for team members is that you get great feedback, and that your career progress is based on your impact.
For the organization, forgoing a separate class of managers ensures a simple structure with clear responsibilities.
It reduces compensation costs, coordination costs, and office politics.
The disadvantage is that your manager has a limited amount of time for you and probably has less experience managing people.
To mitigate these disadvantages we should offer ample training, coaching, support structures, and processes to ensure our managers can handle these tasks correctly and in limited amount of time.
Everyone deserves a great boss that helps you with your career, lets you know when you should improve, hires a great team, and motivates and coaches you to get the best out of you.
