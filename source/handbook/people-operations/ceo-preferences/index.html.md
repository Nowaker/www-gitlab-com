## Intro

This page is mainly as a guideline for the Executive Assistants (EAs) to our Chief Executive Officer (CEO). It is intended to be helpful, feel free to deviate from it and update this page if you think it makes sense. If there are things that might seem pretentious or overbearing please raise them so we can remove or adapt them.

## Communication

Thanks to Mårten Mickos for the inspiration for this section. All good idea's are his, all bad ones mine.

I am a visual person much more than auditory, and I am a top-down person much more than bottom-up. This means that I love written communication: issues, email, Google Docs, and chat. Feel free to send me as many emails and chat messages as you like, and about whatever topics you like.

If you have a great new idea or suggestion for me, I appreciate if you can convey it in a picture or in written words, because I learn by seeing more than I learn by hearing. I don't mind if you send me or point me to plans that are in draft mode or not ready. I am happy if I can give useful feedback early. It doesn’t have to be perfect and polished when presented to me.

In written communication, I appreciate the top-down approach. Set the subject header to something descriptive. Start the email by telling me what the email is about. Only then go into details. Don't mix separate topics in the same email, it is perfectly fine to send two emails at almost the same time. Try to have a concrete proposal so I can just reply with OK if that is possible.

I get many email on which I am only cc'd on, I would very much appreciate if you started emails intended specifically for me with "Sid," or some other salutation that makes it clear that the message is for me.

I have accounts on LinkedIn and Facebook, and I am happy to connect with you on both networks. On LinkedIn, I sometimes send a friend request to you, but on Facebook I will not. I will happily accept your LinkedIn and Facebook friend request, but given that I am the CEO, I don’t want to impose myself on anyone. You can also find me on twitter @sytses.

## Pick your brain meetings

If people want advice on open source, remote work, or other things related to GitLab we'll consider that. If Sid approves of the request we send the following email:

We would love to help but we want to make sure the content is radiated as wide as possible. Can we do the following?

1. We schedule a 50 minute skype/google hangout/office visit.
1. You send an initial list of questions no less than 24 hours in advance that you share via a Google Doc that everyone with the link can edit.
1. It is very much OK to keep adding questions to the doc and to ask questions not in the doc during the interview.
1. You audio record the conversation, please test the technology in advance.
1. You write up a draft post within 48 hours after the interview and submit it to https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/ If that is too hard paste the interview in the Google Doc with the questions.
1. If it is interesting our marketing department will work to publish the post.
1. A great examples of this in action is https://about.gitlab.com/2016/07/14/building-an-open-source-company-interview-with-gitlabs-ceo/ that got to nr. 1 on the fontpage on Hacker News.

The EA should follow up to chase the draft post.

## Email

* Labels: /archive, /respond or /urgent-important
* Prepare draft responses
* Standard reply for recruiters:
“We do not accept solicitations by recruiters, recruiting agencies, headhunters and outsourcing organizations. Please find all info [on our jobs page](https://about.gitlab.com/jobs/#no-recruiters)”

## Scheduling

* [everytimezone.com](http://www.everytimezone.com) can help determine the best time to schedule
* You can add other [calendars](calendar.google.com) on the left, to see when GitLab team members are free to schedule a meeting with
* The [agenda](https://docs.google.com/document/d/187Q355Q4IvrJ-uayVamoQmh0aXZ6eixAOE90jZspAY4/edit?ts=574610db&pli=1) of items to be handled by the EA for the CEO
* Use for example a tool like [Hipmunk](www.hipmunk.com) to find different flight options with most airlines when needing to book travel
* Keep 1 hour open in calendar per day for email
* Schedule calls in European timezones in the am Pacific (Daylight) Time and US time zones in the pm Pacific (Daylight) Time.
    * MTG | for in person
    * INTERVIEW | for interviews (looping in our PR partner)
    Make sure to block 10 min before for preparations and 10 min after for notes for Sid and send out https://about.gitlab.com/primer/ for background information for the interviewer.
    * CALL | for video conf
    add message:
    “Please use the link in this invite to join the Google Hangout. It will prompt a request for access.
	    * Calls in the hiring process also have:
	    “Please fill out [this form](https://docs.google.com/a/gitlab.com/forms/d/1lBq_oXaqpQRs-SeEs3EvpxFGK55Enqn_nzkLq2l3Rwg/viewform) to discuss during the call”

* Make sure to add any relevant details in the invite.
* Examples for scheduling:
“CALL | Kirsten Abma for Executive Assistant”
“INTERVIEW | Kirsten Abma (TechCrunch)”
“MTG | Bruce Armstrong (Khosla) & Sid Sijbrandij (GitLab) @ [office/location]”
Or for example “Dinner/Lunch @ Bar Agricole - Kirsten Abma & Sid Sijbrandij”

For meetings or lunch/dinner appointments, always make sure to add the address in the invite of the location where it’s scheduled and make sure to plan travel time (in a separate calendar item, just for Sid) before and after the meeting in case another meeting or call should follow.

## Hiring process

* Hiring managers will tag EA to schedule a call for the applicant with Sid
* Template email with wording to schedule “Kirsten schedule Sid”
* Add date/time and make sure to include timezones
* Add responses of [form](https://docs.google.com/a/gitlab.com/forms/d/1lBq_oXaqpQRs-SeEs3EvpxFGK55Enqn_nzkLq2l3Rwg/viewform) to our ATS; [Workable](https://gitlab.workable.com/backend)

## Travel

EA does research for the best option for a flight and propose this to Sid before booking.
Current preferences for flights are:
* Aisle seat
* Check a bag for **all** trips (longer than one night)
* Frequent Flyer details of all (previously flown) airlines are in EA vault of 1Password

## Mail

* Check all incoming (physical) mail at 1233 Howard st and sort the urgent and important letters.
* Inform AP if invoices came in
* Inform other people/departments if mail is addressed for them and include a scan of the document

## Expensify

* When you’re logged in, you can find wingman account access for Sid in the top right corner menu.
* Check Sids email, using the search bar in the top, to find any receipts for the postings in the current expense report.
* Write down what receipts are missing and email to request them
