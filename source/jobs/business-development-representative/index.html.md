---
layout: job_page
title: "Business Development Representative"
---

You love talking about GitLab to people and no question or challenge is too big or small. You have experience working directly with customers in order to answer questions on getting started with a technical product. Your job is to make sure our customers are successful, from the single-shop development firm all the way up to our Fortune 500 customers, and that everyone gets the appropriate level of support.

## Responsibilities

* Strategize with our demand generation manager to develop the proper qualifying questions for all type of customers. Be able to identify where a customer is in the sale and marketing funnel and take the appropriate action.
* Develop and work with our demand generation manager to follow the process from which leads pass onto sales, support, etc.
* Participate in documenting all processes in the handbook and update as needed with our demand generation manager.
* Work closely with support to build a process where we can identify customers that would be interesting for EE and pass those onto sales.
* Manage and help inbound requests to community@gitlab.com and sales@gitlab.com.
* Collaborate with our CMO and demand generation manager for all outbound messaging for demand campaigns.
* Engage with our demand generation manager and developer marketing manager to create a signup email campaign for all users of GitLab.com. The goal of this campaign is to introduce more users to our on-premises offerings.
* Own the competitive analysis and comparison page for GitLab. Work closely with sales to identify new competitive technologies and systems and understanding the drawback and benefits.
* Work closely with our developer marketing manager to identify customer stories from all of the conversations you have with our customers.

## Requirements

* Excellent spoken and written English
* Experience in sales, marketing, or customer service
* Experience with CRM and/or marketing automation software is highly preferred
* An understanding of B2B software, Open Source software, and the developer product space is preferred
* Is your college degree in French foreign politics with a minor in interpretive dance but you’ve been hacking together websites since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.
* You are obsessed with making customers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
* Be ready to learn how to use GitLab and Git
* Start part-time or full-time depending on situation
* You share our [values](/handbook/#values), and work in accordance with those values.
