---
layout: job_page
title: "People Operations Generalist"
---

## Responsibilities

- Responsible for the day to day administration of People Operations, and assisting team members with their People Operations questions.
- Assist the Sr. Director of People Operations to develop the strategic direction for People Ops, and implement the functional steps to achieve those goals.
- Develop and implement HR policies, from recruitment and pay, to diversity and employee relations.
- Prepare contracts for employees and contractors on quick turnaround and follow-through with smooth onboarding of new team members.
- Coordinate with our payroll and benefits providers, and using our People Ops Information System and ATS; currently BambooHR and Workable.
- Assist and advise managers in delicate and difficult People Operations issues (special circumstances, conflicts, sickness, layoffs, etc.)
- Create People Operations processes and strategies following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Keep it efficient and DRY.
- Provide assistance to the team with miscellaneous support tasks.
- Maintain the hiring pipeline and ensure proper follow-up. Participate in recruiting, screening, and reference calls.
- Suggest and implement improvements to People Operations, for example for performance reviews and various types of training.
- Help write job descriptions and promotion criteria.
- TODO

## Requirements

- Prior extensive experience in an HR or People Operations role
- Clear understanding of HR laws in one or multiple countries where GitLab is active
- Ability to work strange hours when needed (for example, to call an embassy in a different continent)
- Excellent written and verbal communication skills
- Enthusiasm for, and broad experience with, software tools
- Proven experience quickly learning new software tools
- Willing to work with git and GitLab whenever possible
- Willing to make People Operations as open and transparent as possible
- Wanting to work for a fast moving startup
- You share our [values](/handbook/#values), and work in accordance with those values
- The ability to work in a fast paced environment with strong attention to detail is essential.
- TODO
